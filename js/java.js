const btnMostrar = document.getElementById("btnMostrar");
const btnLimpiar = document.getElementById("btnLimpiar");
const containerData = document.querySelector("#container__data");
const templateData = document.getElementById("templateData");
const fragment = document.createDocumentFragment();
const Tganancia = document.getElementById("Tganancia");
const Totalganancia = document.getElementById("Totalganancia");

function cargarAjax() {
    const url = "/html/servicio.json";

    axios
        .get(url)
        .then((res) => {
            mostrar(res.data);
        })
        .catch((reject) => {
            console.log("Surgio un error" + reject);
        });
}

function mostrar(data) {
    let tg = 0;
    for (let item of data) {
        const clone = templateData.content.cloneNode(true);
        clone.querySelector("#id").textContent = item.idcliente;
        clone.querySelector("#codigo").textContent = item.codigo;
        clone.querySelector("#descripcion").textContent = item.descripcion;
        clone.querySelector("#PrecioVenta").textContent = item.preciovta;
        clone.querySelector("#PrecioCompra").textContent = item.preciocompra;
        clone.querySelector("#cantidad").textContent = item.cantidad;
        clone.querySelector("#Ganancias").textContent = (item.preciovta - item.preciocompra) * item.cantidad;
        tg += parseFloat(clone.querySelector("#Ganancias").textContent);
        fragment.appendChild(clone);
    }
    
    containerData.innerHTML = "";
    containerData.appendChild(fragment);
    const servicio = Object.keys(data).length;
    Tganancia.innerHTML = "Ganancia Total: " + (tg + servicio).toFixed(2);

}
btnMostrar.addEventListener("click", cargarAjax);

btnLimpiar.addEventListener("click", () => {
    containerData.innerHTML = "";
    Tganancia.innerHTML = "Ganancia Total:";
});
